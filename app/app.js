(function (){
	'user strict';

    angular.module('portal', [
        'ngResource',
        'ui.bootstrap',
        'portal.profile',
        'portal.login',
        'portal.profiles',
        'portal.service',
        'portal.user',
        'pascalprecht.translate',
        'portal.team',
        'checklist-model'
    ])

  .config(function ($translateProvider) {
		$translateProvider.useStaticFilesLoader({
		  prefix: '/app/static/data/language/locale-',
		  suffix: '.json'
		});
		$translateProvider.preferredLanguage('default');
		$translateProvider.useSanitizeValueStrategy('escape');
	})

  .config(function ($stateProvider, $urlRouterProvider, $qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
    $urlRouterProvider.otherwise('/');
    $stateProvider
      .state('index', {
        abstract: true,
        views: {
          'sidebar': {
            templateUrl: 'modules/portal/sidebar/partials/sidebar.html',
            controller: "SidebarCtrl",
            controllerAs: 'vm'
          },
          'navbar': {
            templateUrl: 'modules/portal/navbar/partials/navbar.html'
          },
          'footer' : {
            templateUrl: 'static/partials/footer.html'
          }
        }
      })
      .state('loginView', {
        abstract: true,
        views: {
          'navbar': {
            templateUrl: 'static/partials/navbar.html'
          },
          'footer' : {
            templateUrl: 'static/partials/footer.html'
          }
        }
      })
    }
  )

  .run(function ($translate) {
    $translate.use("default");
    //$rootScope.language = successResult.ActiveConfiguration;
/*
    $rootScope.domainUrl = '192.168.0.100:12316';
      
    Language.get({},
      function (successResult) {
        $translate.use(successResult.ActiveConfiguration);
        $rootScope.language = successResult.ActiveConfiguration;
      },
      function (errorResult) {
      }
    );*/
  })
    .run(function ($rootScope) {
        $rootScope.domainUrl = 'http://localhost:8080/app';
    })
	/*.factory('Language', ['$resource', '$rootScope', 
  	function ($resource, $rootScope) {
    	return $resource('http://192.168.0.100:12316/api/configuration/getactiveconfiguration');
    }]);*/
})();