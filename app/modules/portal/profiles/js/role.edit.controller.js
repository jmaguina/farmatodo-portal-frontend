(function(){
  'use strict';

  angular
    .module('portal.profiles')
    .controller('RoleEditCTRL', RoleEditCTRL);

  RoleEditCTRL.$inject = ['Roles', 'Apps', 'common', '$state'];
  function RoleEditCTRL(Roles, Apps, common, $state){
    var vm = this;
    vm.error = false;
    vm.role = common.role;

    Apps.query({},
      function(success){
        vm.role.apps = success;
      }
    );

    /*Roles.get({},
      function(success){
        vm.user = success;
      },
      function(error){
        vm.error = true;
      }
    );*/
    
    vm.back = function () {
      $state.go('role');
    };

  };
})();