(function () {
    'use strict';

    angular
        .module('portal.profiles')
        .controller('ProfilesCTRL', ProfilesCTRL)
        .controller('ProfileAddCTRL', ProfileAddCTRL)
        .controller('ProfilesDetailsCTRL', ProfilesDetailsCTRL);

    ProfilesCTRL.$inject = ['Profiles', '$state', 'common', '$scope', '$modal'];
    function ProfilesCTRL(Profiles, $state, common, $scope, $modal) {
        var vm = this;
        vm.button = false;
        vm.error = false;
        vm.statusSuccess = true;

        Profiles.query({},
            function (success) {
                vm.profiles = success;
            },
            function (error) {
                vm.error = true;
            }
        );

        vm.profilesEdit = function (profiles) {
            common.profiles = profiles;
            $state.go('profilesEdit');
        };

        vm.profilesMassive = function (profiles) {
            common.profiles = profiles;
            $state.go('profilesMassive');
        };


        vm.profilesStatusModal = function (profiles) {
            vm.status = profiles;
            $scope.modalInstance = $modal.open({
                templateUrl: 'modules/portal/profiles/partials/modal/status.html',
                scope: $scope,
                backdrop: 'static',
                size: 'sm',
                keyboard: false
            });
        };

        vm.showDetail = function (profiles) {
            vm.detail = profiles;
            $scope.modalInstance = $modal.open({
                templateUrl: 'modules/portal/profiles/partials/modal/detail.html',
                scope: $scope,
                backdrop: 'static',
                size: 'sm',
                keyboard: false
            });
        };

        vm.ok = function () {
            $scope.modalInstance.dismiss('cancel');
        };

        vm.cancel = function () {
            $scope.modalInstance.dismiss('cancel');
        };

        vm.modifyStatus = function (profiles) {
            vm.statusSuccess = false;
            vm.button = true;
        };

    }

    ProfilesDetailsCTRL.$inject = ['ProfilesDetails', '$state', 'common', '$scope', '$modal','Permits','ProfileData','User','Team'];
    function ProfilesDetailsCTRL(ProfilesDetails, $state, common, $scope, $modal,Permits,ProfileData,User,Team) {
        var vm = this;
        vm.button = false;
        vm.error = false;
        vm.statusSuccess = true;

        Permits.query({},
            function (success) {
                vm.permits = success;
            },
            function (error) {
                vm.error = true;
            }
        );

        User.query(
            function (data){
                vm.user = data;
            },
            function() {
                vm.error = true;
            }
        )

        Team.query({},
            function(success){
                vm.teams = success;
            },
            function (error) {
                vm.error = true;
            }
        );

        vm.addProfiles = ProfileData.get({},
            function(success){
                return success;
            },
            function (error){
                return error;
            }
        );

/*        ProfilesDetails.get({},
            function (success) {
                vm.profilesDetail = success;
            },
            function (error) {
                vm.error = true;
            }
        );*/

        vm.userDetailsModal = function (user) {
            vm.user = user;
            $scope.modalInstance = $modal.open({
                templateUrl: 'modules/portal/profiles/partials/modal/user_detail.html',
                scope: $scope,
                backdrop: 'static',
                size: 'sm',
                keyboard: false
            });
        };

        vm.cerrar = function () {
            $scope.modalInstance.dismiss('cancel');
        };
    }

    ProfileAddCTRL.$inject = ['ProfileAdd','ProfileData', '$state', 'common', '$scope', '$modal','Permits'];
    function ProfileAddCTRL(ProfileAdd,ProfileData, $state, common, $scope, $modal,Permits) {
        var vm = this;
        vm.error= false;
        vm.insercion = false;
        vm.permitType = [];

        Permits.query({},
            function (success) {
                vm.permits = success;
            },
            function (error) {
                vm.error = true;
            }
        );

        vm.addProfiles = ProfileData.get({},
                function(success){
                    return success;
                },
                function (error){
                    return error;
                }
            );

        vm.ok = function () {
            $scope.modalInstance.dismiss('cancel');
            $state.go('profiles')
        };

        vm.submit= function(){
            ProfileAdd.get({},
                function (success) {
                    vm.insercion = success;
                },
                function (error) {
                    vm.error = true;
                });
            $scope.modalInstance = $modal.open({
                templateUrl: 'modules/portal/profiles/partials/modal/add.html',
                scope: $scope,
                backdrop: 'static',
                size: 'sm',
                keyboard: false
            });
        }
    }
})();