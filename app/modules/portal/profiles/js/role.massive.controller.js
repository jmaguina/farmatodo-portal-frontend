(function(){
  'use strict';

  angular
    .module('portal.profiles')
    .controller('RoleMassiveCTRL', RoleMassiveCTRL);

  RoleMassiveCTRL.$inject = ['User', 'common', '$state', '$modal', '$scope', '$filter', 'Role'];
  function RoleMassiveCTRL(User, common, $state, $modal, $scope, $filter, Role){
    var vm = this;
    vm.button = false;
    vm.error = false;
    vm.role = null;
    vm.roles = [];
    vm.selectedUsers = [];
    vm.statusSuccess = true;
    vm.message = true;

    User.query({},
      function(success){
        vm.user = success;
      },
      function(error){
        vm.error = true;
      }
    );

    Role.query({},
      function(success){
        vm.roles = success;
      },
      function(error){
        vm.error = true;
      }
    );

    vm.selectUser = function (user){
      //resul hold a new array with all the elements that have the same cedula of the user selected in the menu
      var result = $filter('filter')(vm.selectedUsers, {cedula : user.cedula}, true);
      //if the length is 0 then the object is not present in the users array
      if (result.length == 0) vm.selectedUsers.push(user);
      //if the length is not 0, then the element exists and the checbok is being unselected, so the element needs to be removed from the array.
      else {
        for (var i=0;i<vm.selectedUsers.length;i++){
          if(vm.selectedUsers[i].cedula==user.cedula){
            vm.selectedUsers.splice(i,1);
            break;
          }
        }
      }
    };

    vm.addRoleMassiveModal = function (){
      $scope.modalInstance = $modal.open({
        templateUrl:'modules/portal/profiles/partials/modal/add_role_massive.html',
        scope: $scope,
        backdrop: 'static',
        size: 'md',
        keyboard  : false
      });
    };

    vm.removeRoleMassiveModal = function (){
      $scope.modalInstance = $modal.open({
        templateUrl:'modules/portal/profiles/partials/modal/remove_role_massive.html',
        scope: $scope,
        backdrop: 'static',
        size: 'md',
        keyboard  : false
      });
    };

    vm.addRoleMassive = function (){
      vm.message = false;
      for (var i=0; i<vm.selectedUsers.length; i++){
        if(vm.selectedUsers[i].role!=vm.role){
          vm.selectedUsers[i].role=vm.role;
        }
      } 
    };

    vm.removeRoleMassive = function (){
      vm.message = false;
      for (var i=0; i<vm.selectedUsers.length; i++){
        if(vm.selectedUsers[i].role==vm.role){
          vm.selectedUsers[i].role=null;
        }
      } 
    };

    vm.ok = function () {
      $scope.modalInstance.dismiss('cancel');
      vm.message = true;
    };

    vm.cancel = function () {
      $scope.modalInstance.dismiss('cancel');
      vm.message = true;
    };

  };
})();