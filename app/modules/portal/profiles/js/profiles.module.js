(function () {
    'use strict';

    angular
        .module("portal.profiles", ['ui.router'])
        .run(addStateToScope)
        .config(getRoutes);


    addStateToScope.$inject = ['$rootScope', '$state', '$stateParams'];
    function addStateToScope($rootScope, $state, $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    };

    getRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];
    function getRoutes($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('profiles', {
                url: '/profiles',
                parent: 'index',
                views: {
                    'content@': {
                        templateUrl: "modules/portal/profiles/partials/profiles.html",
                        controller: "ProfilesCTRL",
                        controllerAs: 'vm'
                    }
                }
            })
            .state('profiles_details', {
                url: '/profiles_details',
                parent: 'index',
                views: {
                    'content@': {
                        templateUrl: "modules/portal/profiles/partials/profiles_details.html",
                        controller: "ProfilesDetailsCTRL",
                        controllerAs: 'vm'
                    }
                }
            })
            .state('roleEdit', {
                url: '/roleEdit',
                parent: 'index',
                views: {
                    'content@': {
                        templateUrl: "modules/portal/profiles/partials/role_edit.html",
                        controller: "RoleEditCTRL",
                        controllerAs: 'vm'
                    }
                }
            })
            .state('roleAdd', {
                url: '/roleAdd',
                parent: 'index',
                views: {
                    'content@': {
                        templateUrl: "modules/portal/profiles/partials/role_add.html",
                        controller: "RoleAddCTRL",
                        controllerAs: 'vm'
                    }
                }
            })
            .state('roleMassive', {
                url: '/roleMassive',
                parent: 'index',
                views: {
                    'content@': {
                        templateUrl: "modules/portal/profiles/partials/role_massive.html",
                        controller: "RoleMassiveCTRL",
                        controllerAs: 'vm'
                    }
                }
            })
            .state('profileAdd', {
                url: '/profileAdd',
                parent: 'index',
                views: {
                    'content@': {
                        templateUrl: "modules/portal/profiles/partials/profile_add.html",
                        controller: "ProfileAddCTRL",
                        controllerAs: 'vm'
                    }
                }
            })
    };
})();