(function(){
  'use strict';

  angular
    .module('portal.profiles')
    .controller('RoleAddCTRL', RoleAddCTRL);

  RoleAddCTRL.$inject = ['Roles', 'Apps', 'common', '$state', '$scope', '$modal', '$timeout'];
  function RoleAddCTRL(Roles, Apps, common, $state, $scope, $modal, $timeout){
    var vm = this;
    vm.button = false;
    vm.failure = false
    vm.process = false;
    vm.success = false;
    vm.role = {};

    Apps.query({},
      function (success){
        vm.role.apps = success;
      }
    );

    /*
    vm.submit = function () = {
    vm.process = true;
    $scope.modalInstance = $modal.open({
      templateUrl: 'modules/portal/user/partials/modal/register.html',
      scope: $scope,
      size: 'sm',
      backdrop: 'static',
      keyboard  : false
    });
    User.save({},vm.user,
      function(success){
        vm.button = true;
        vm.process = false;
        vm.success = true;
      },
      function(error){
        vm.button = true;
        vm.process = false;
        vm.success = false;  
      });
    };
    */ 

    vm.submit = function () {
      vm.process = true;
      $scope.modalInstance = $modal.open({
        templateUrl: 'modules/portal/profiles/partials/modal/add.html',
        scope: $scope,
        size: 'sm',
        backdrop: 'static',
        keyboard  : false
      });
      $timeout(function () {
        vm.button = true;
        vm.process = false;
        vm.success = true;
      }, 5000);  
    };

    vm.back = function () {
      $state.go('role');
    };

    vm.ok = function () {
      $scope.modalInstance.dismiss('cancel');
      $state.go('role');
    };

  };
})();