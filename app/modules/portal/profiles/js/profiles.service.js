(function () {
    'use strict';

    angular
        .module('portal.profiles')
        .factory('Apps', Apps)
        .factory('Role', Role)
        .factory('Profiles', Profiles)
        .factory('ProfilesDetails', ProfilesDetails)
        .factory('ProfileAdd', ProfileAdd)
        .factory('ProfileData', ProfileData)
        .factory('Permits', Permits)
        .value('common', {});

    Apps.$inject = ['$resource','$rootScope'];
    function Apps($resource,$rootScope) {
        return $resource($rootScope.domainUrl+'/static/data/apps.json');
    }

    Role.$inject = ['$resource','$rootScope'];
    function Role($resource,$rootScope) {
        return $resource($rootScope.domainUrl+'/static/data/role.json');
    }

    Profiles.$inject = ['$resource','$rootScope'];
    function Profiles($resource,$rootScope) {
        return $resource($rootScope.domainUrl+'/static/data/profiles.json');
    }

    ProfilesDetails.$inject = ['$resource','$rootScope'];
    function ProfilesDetails($resource,$rootScope) {
        return $resource($rootScope.domainUrl+'/static/data/profiles_detail.json');
    }

    ProfileAdd.$inject = ['$resource','$rootScope'];
    function ProfileAdd($resource,$rootScope) {
        return $resource($rootScope.domainUrl+'/static/data/profile_add.json');
    }

    ProfileData.$inject = ['$resource','$rootScope'];
    function ProfileData($resource,$rootScope) {
        return $resource($rootScope.domainUrl+'/static/data/profiles_data.json');
    }

    Permits.$inject = ['$resource','$rootScope'];
    function Permits($resource,$rootScope) {
        return $resource($rootScope.domainUrl+'/static/data/permits.json');
    }
})();