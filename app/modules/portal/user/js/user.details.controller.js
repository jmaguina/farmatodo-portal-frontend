/**
 * Created by posma-kevin on 26/04/2017.
 */
(function(){
    angular
        .module('portal.user')
        .controller('UserDetailCTRL', UserDetailCTRL);

    UserDetailCTRL.$inject = ['UserDetail','Group','common','$state', '$scope', '$modal', '$timeout','Countries', 'Profile'];
    function UserDetailCTRL (UserDetail,Group,common,$state, $scope,$modal, $timeout,Countries, Profile){
        var vm = this;
        vm.allGroups = [];
        vm.allProfiles = [];
        vm.user = {};
        vm.groupsSelected = [];
        vm.profiles = [];
        vm.groupSaved = [];
        vm.profileSaved = [];
        vm.countryPreferences =[];
        vm.success = false;
        vm.successProfile = false;
        vm.successGroup = false;
        vm.showNewProfile = false;
        vm.profilesNew = [];

        vm.tab = 1;

        /*vm.tab = 1;*/

        /*vm.setTab = function (tabId) {
            vm.tab = tabId;

            if (tabId == 2)
                vm.loadGroups();

            if (tabId == 3)
                vm.loadProfiles();

        };*/

       /* vm.isSet = function (tabId) {
            return vm.tab === tabId;
        };*/


        UserDetail.get(
            function (data){
                vm.user = data.user;
                vm.groupsSelected = data.groups;
                vm.profiles = data.profiles;
                vm.profilesNew = vm.profiles.slice(0,vm.profiles.length);
            },
            function() {
                //console.log("Error consultando los datos del usuario");
            }
        );

        Group.query(
            function (data) {
                vm.allGroups = data;
            },
            function () {
                //console.log("Error consultando los grupos")
            }
        );

        Countries.query(
            function (data) {
                vm.countries = data;
            },
            function () {
                //console.log("Error consultando los grupos")
            }
        );

        Profile.query(
            function(data) {
                vm.allProfiles = data;
            },
            function () {}
        );

        /*vm.loadGroups = function(){
            angular.forEach(vm.allGroups, function(grupos,key){
                angular.forEach(vm.groupsSelected,function(value,key){
                    if(value.id == grupos.id){
                        grupos.selected = true;
                    }
                });
            });
        };*/

       /* vm.loadProfiles = function() {
            angular.forEach(vm.profiles, function(profile,key){
                profile.selected = true;
            });
        };*/

        //Levanta el modal que notifica si quiere guardar los grupos seleccionados
        vm.saveGroup = function() {
            $scope.modalInstance = $modal.open({
                templateUrl: 'modules/portal/user/partials/modal/user_add_group.html',
                scope: $scope,
                size: 'sm',
                backdrop: 'static',
                keyboard: false
            });
        };

        //Levanta el modal que notifica si quiere guardar los perfiles
        vm.saveProfile = function() {
            vm.success = true;
            $scope.modalInstance = $modal.open({
                templateUrl: 'modules/portal/user/partials/modal/user_add_profile.html',
                scope: $scope,
                size: 'sm',
                backdrop: 'static',
                keyboard: false
            });

        };

        //Levanta el modal paranotificar que se va a borrar un perfil
        vm.removeProfile = function(){
            $scope.modalInstance = $modal.open({
                templateUrl: 'modules/portal/user/partials/modal/user_delete.html',
                scope: $scope,
                size: 'sm',
                backdrop: 'static',
                keyboard: false
            });

        };

        //Levanta el modal para notificar que se va a guardar el pais permitido
        vm.saveCountryPreferences = function () {
            $scope.modalInstance = $modal.open({
                templateUrl: 'modules/portal/user/partials/modal/user_save_country.html',
                scope: $scope,
                size: 'sm',
                backdrop: 'static',
                keyboard: false
            });
        };

        vm.deleteProfile = function(){

            /*            DeleteProfile.save(
             function (data) {
             //Manejos del eliminado del perfil
             },
             function () {
             //Manejo del error al eliminar un perfil
             }
             );*/

            $scope.modalInstance.dismiss('cancel');
        };

        vm.searchProfiles = function(){

/*            ProfileSearch.query({},vm.profile,
                function(success){
                    $timeout(function () {


                    }, 2500);
                },
                function(error){

                }
            )*/

        };

        //Oculta/Muestra la tabla para a�adir/agregar perfiles nuevos al usuario
        vm.newProfile = function() {
            vm.showNewProfile = true;
        };

        //TODO llamar al servicio que guarda los cambios en BD
        vm.addProfile = function() {
            vm.profiles = [];
            $scope.modalInstance.dismiss('cancel');
            vm.showNewProfile = false;
            vm.profiles = vm.profilesNew;
        };

        // TODO LLamar al servicio que guarda los cambios en lalista de grupos
        vm.addGroup = function () {
            $scope.modalInstance.dismiss('cancel');
        };

        vm.ok = function() {
            $scope.modalInstance.dismiss('cancel');
        };

        vm.back = function() {
            vm.showNewProfile = false;
        };



    };

})();
