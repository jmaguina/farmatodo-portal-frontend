(function(){
  'use strict';

  angular
  .module("portal.user", ['ui.router'])
  .run(addStateToScope)
  .config(getRoutes);


  addStateToScope.$inject = ['$rootScope', '$state', '$stateParams'];
    function addStateToScope($rootScope, $state, $stateParams){
      $rootScope.$state = $state;
      $rootScope.$stateParams = $stateParams;
    };

  getRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];
    function getRoutes($stateProvider, $urlRouterProvider){

  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('user', {
      url: '/user',
      parent: 'index',
      views:{
        'content@': {
          templateUrl: "modules/portal/user/partials/user.html",
          controller: "UserCTRL",
          controllerAs: 'vm'
        }
      }
    })
    .state('userEdit', {
      url: '/userEdit',
      parent: 'index',
      views:{
        'content@': {
          templateUrl: "modules/portal/user/partials/user_edit.html",
          controller: "UserEditCTRL",
          controllerAs: 'vm'
        }
      }
    })
    .state('userRegister', {
      url: '/userRegister',
      parent: 'index',
      views:{
        'content@': {
          templateUrl: "modules/portal/user/partials/user_register.html",
          controller: "UserRegisterCTRL",
          controllerAs: 'vm'
        }
      }
    })
    .state('userDetail', {
        url:'/userDetail',
        parent: 'index',
        views:{
          'content@': {
            templateUrl: "modules/portal/user/partials/user_details.html",
            controller: 'UserDetailCTRL',
            controllerAs: 'vm'
          }
        }
    })
  };
})();