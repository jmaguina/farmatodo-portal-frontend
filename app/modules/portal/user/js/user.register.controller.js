(function(){
  'use strict';

  angular
    .module('portal.user')
    .controller('UserRegisterCTRL', UserRegisterCTRL);

  UserRegisterCTRL.$inject = ['UserAuthorize', 'common', '$state', '$scope', '$modal', '$timeout', 'TeamResult'];
  function UserRegisterCTRL(UserAuthorize, common, $state, $scope, $modal, $timeout, TeamResult){
    var vm = this;
    vm.button = false;
    vm.failure = false
    vm.process = false;
    vm.success = false;
    vm.authorized = false;
    vm.nonexistent = false;
    vm.user = common.user;

/*    vm.submit = function () {
      vm.process = true;
      $scope.modalInstance = $modal.open({
        templateUrl: 'modules/portal/user/partials/modal/register.html',
        scope: $scope,
        size: 'sm',
        backdrop: 'static',
        keyboard  : false
      });
      $timeout(function () {
        vm.button = true;
        vm.process = false;
        vm.success = true;
      }, 5000);
    };*/


    vm.submit = function () {
      vm.process = true;
     $scope.modalInstance = $modal.open({
     templateUrl: 'modules/portal/user/partials/modal/register.html',
     scope: $scope,
     size: 'sm',
     backdrop: 'static',
     keyboard  : false
     });
      UserAuthorize.get({},
          function(success){
            vm.response = success;

            $timeout(function () {

              if(vm.response.identifier == vm.user.name){
                vm.button = true;
                vm.process = false;
                vm.authorized = true;
              }else{
                if(vm.response.identifier == ""){
                  vm.button = true;
                  vm.process = false;
                  vm.nonexistent = true;
                }else{
                  vm.button = true;
                  vm.process = false;
                  vm.success = true;
                }
              }
            }, 5000);


          },
          function(error){
            vm.button = true;
            vm.process = false;
            vm.failure = true;
            //Manejo de Errores
          }
      );
    };

    vm.back = function () {
      $state.go('user');
    };

    vm.ok = function () {
      $scope.modalInstance.dismiss('cancel');
      $state.reload();
    };

  };
})();