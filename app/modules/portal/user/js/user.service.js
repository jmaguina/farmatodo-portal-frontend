(function(){
	'use strict';

	angular
		.module('portal.user')
		.factory('User', User)
		.factory('UserAuthorize', UserAuthorize)
		.factory('Categories', Categories)
		.factory('Groups', Groups)
		.factory('UserSearch', UserSearch)
		.factory('UserDetail',UserDetail)
		.factory('Group', Group)
		.factory('Countries', Countries)
		.factory('Profile', Profile)
		.value('common',{});

	User.$inject = ['$resource','$rootScope'];
	function User($resource,$rootScope){
		return $resource($rootScope.domainUrl+'/static/data/users.json');
	};

	UserAuthorize.$inject = ['$resource','$rootScope'];
	function UserAuthorize($resource,$rootScope){
		return $resource($rootScope.domainUrl+'/static/data/authorize.json');
	};

	Categories.$inject = ['$resource','$rootScope'];
	function Categories($resource,$rootScope){
		return $resource($rootScope.domainUrl+'/static/data/categories.json');
	};

	Groups.$inject = ['$resource','$rootScope'];
	function Groups($resource,$rootScope){
		return $resource($rootScope.domainUrl+'/static/data/groups.json');
	};

	UserSearch.$inject = ['$resource','$rootScope'];
	function UserSearch($resource,$rootScope){
		return $resource($rootScope.domainUrl+'/static/data/users.json');
	};
	
	UserDetail.$inject = ['$resource','$rootScope'];
	function UserDetail($resource,$rootScope) {
		return $resource($rootScope.domainUrl+'/static/data/userdetails.json');
	};

	Group.$inject = ['$resource','$rootScope'];
	function Group($resource,$rootScope) {
		return $resource($rootScope.domainUrl+'/static/data/groups.json');
	};

	Countries.$inject = ['$resource','$rootScope'];
	function Countries($resource,$rootScope) {
		return $resource($rootScope.domainUrl+'/static/data/countries.json');
	};

	Profile.$inject = ['$resource', '$rootScope'];
	function Profile($resource, $rootScope) {
		return $resource($rootScope.domainUrl+'/static/data/profiles.json');
	};

})();