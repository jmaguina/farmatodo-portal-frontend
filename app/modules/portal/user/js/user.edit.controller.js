(function(){
  'use strict';

  angular
    .module('portal.user')
    .controller('UserEditCTRL', UserEditCTRL);

  UserEditCTRL.$inject = ['User', 'common', '$state'];
  function UserEditCTRL(User, common, $state){
    var vm = this;
    vm.error = false;
    vm.user = common.user;

    /*User.get({},
      function(success){
        vm.user = success;
      },
      function(error){
        vm.error = true;
      }
    );*/
    
    vm.back = function () {
      $state.go('user');
    };

  };
})();