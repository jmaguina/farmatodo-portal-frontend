(function(){
  'use strict';

  angular
    .module('portal.user')
    .controller('UserCTRL', UserCTRL);

  UserCTRL.$inject = ['User', 'UserSearch','common', '$state', '$modal', '$scope','Categories','Groups','$timeout'];
  function UserCTRL(User, UserSearch, common, $state, $modal, $scope,Categories,Groups,$timeout){
    var vm = this;
    vm.button = false;
    vm.error = false;
    vm.searching = false;
    vm.statusSuccess = true;

/*    User.query({},
      function(success){
        vm.user = success;
      },
      function(error){
        vm.error = true;
      }
    );*/

    Categories.query({},
        function(success){
          vm.categories = success
          vm.defaultCategory = vm.categories[0].name;
        },
        function(error){
          //Manejo de Errores
        }
    );

    Groups.query({},
        function(success){
          vm.groups = success
          vm.defaultGroup = vm.groups[0].name;
        },
        function(error){
          //Manejo de Errores
        }
    );

      vm.searchUsers = function(){
        vm.userList = [];
        vm.searching = true;
        vm.searchValues ={
            "category": vm.defaultCategory,
            "group":vm.defaultGroup,
            "user": vm.user
        }
          //Simulacion de una b�squeda por filtros!
          UserSearch.query({},vm.searchValues,
              function(success){
                  $timeout(function () {
                      vm.searching = false;
                      vm.userList = success;

                  }, 2500);
              },
              function(error){
                vm.searching = false;
                vm.error = true;
              }
          )
      };

    vm.userEdit = function (user){
      common.user = user;
      $state.go('userEdit');
    };

    vm.userStatusModal = function (role){
      vm.status = role;
      $scope.modalInstance = $modal.open({
        templateUrl:
            'modules/portal/user/partials/modal/status.html',
        scope: $scope,
        backdrop: 'static',
        size: 'sm',
        keyboard  : false
      });
    };

    vm.modifyStatus = function () {
      vm.statusSuccess = false;
      vm.button = true;
    };

    vm.showDetail = function(){
      $state.go('userDetail');
    };

    vm.ok = function () {
      $scope.modalInstance.dismiss('cancel');
      //$state.reload();
    };

    vm.saveCountry = function(){

    };

    vm.cancel = function () {
      $scope.modalInstance.dismiss('cancel');
    };

    vm.createUser = function(){
      $state.go('userRegister');
    };

  };
})();