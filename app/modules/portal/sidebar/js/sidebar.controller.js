(function(){
  'use strict';

  angular
    .module('portal')
    .controller('SidebarCtrl',SidebarCtrl)

  SidebarCtrl.$inject = ['$scope', 'Sidebar','$state'];
  function SidebarCtrl ($scope, Sidebar, $state) {
    $scope.go = function(item){
      $state.go(item.state);
    };

    Sidebar.query({},
      function(success){
        $scope.items = success;
      },
      function(error){
        vm.error = true;
      }
    );
  }
})();