(function(){
	'use strict';

	angular
		.module('portal')
		.factory('Sidebar', Sidebar);

	Sidebar.$inject = ['$resource','$rootScope'];
	function Sidebar($resource,$rootScope){
		return $resource($rootScope.domainUrl+'/static/data/sidebar.json');
	};
})();