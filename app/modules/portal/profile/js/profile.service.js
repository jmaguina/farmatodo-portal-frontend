(function(){
	'use strict';

	angular
		.module('portal.profile')
		.factory('UserProfile', UserProfile);

	UserProfile.$inject = ['$resource','$rootScope'];
	function UserProfile($resource,$rootScope){
		return $resource($rootScope.domainUrl+'/static/data/profile.json');
	};
})();