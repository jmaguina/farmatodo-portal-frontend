(function(){
  'use strict';

  angular
    .module('portal.profile')
    .controller('ProfileCTRL', ProfileCTRL);

  ProfileCTRL.$inject = ['UserProfile', '$state'];
  function ProfileCTRL(UserProfile, $state){
    var vm = this;
    vm.error = false;

    UserProfile.get({},
      function(success){
        vm.profile = success;
      },
      function(error){
        vm.error = true;
      }
    );

  };
})();