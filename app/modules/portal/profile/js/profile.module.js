(function(){
  'use strict';

  angular
  .module("portal.profile", ['ui.router'])
  .run(addStateToScope)
  .config(getRoutes);


  addStateToScope.$inject = ['$rootScope', '$state', '$stateParams'];
    function addStateToScope($rootScope, $state, $stateParams){
      $rootScope.$state = $state;
      $rootScope.$stateParams = $stateParams;
    };

  getRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];
    function getRoutes($stateProvider, $urlRouterProvider){

  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('profile', {
      url: '/profile',
      parent: 'index',
      views:{
        'content@': {
          templateUrl: "modules/portal/profile/partials/profile.html",
          controller: "ProfileCTRL",
          controllerAs: 'vm'
        }
      }
    })
  };
})();