(function() {
    'use strict';

    angular
        .module("portal.team", ['ui.router'])
        .run(addStateToScope)
        .config(getRoutes);


    addStateToScope.$inject = ['$rootScope', '$state', '$stateParams'];
    function addStateToScope($rootScope, $state, $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    };

    getRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];
    function getRoutes($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
          .state('team', {
            url: '/team',
            parent: 'index',
            views:{
              'content@':{
                templateUrl: "modules/portal/team/partials/list_team.html",
                controller: "TeamCTRL",
                controllerAs: 'vm'
              }
            }
          }
        )
          .state('teamRegister', {
           url:'/teamRegister',
           parent:'index',
           views:{
              'content@': {
                templateUrl: "modules/portal/team/partials/team_register.html",
                controller: "TeamRegisterCTRL",
                controllerAs: 'vm'
               }
          }

        })
            .state('teamEdit', {
                url:'/teamEdit',
                parent:'index',
                views:{
                    'content@': {
                        templateUrl: "modules/portal/team/partials/team_edit.html",
                        controller: "TeamEditCTRL",
                        controllerAs: 'vm'
                    }
                }

            })
        .state('teamDetail', {
            url: "/teamDetail",
            parent: 'index',
            views: {
                'content@': {
                    templateUrl: "modules/portal/team/partials/team_detail.html",
                    controller: "TeamDetailCTRL",
                    controllerAs: 'vm'
                }
            }
        })

            .state('teamAddNewProfile', {
                url: "/teamAddNewProfile",
                parent: 'index',
                views: {
                    'content@': {
                        templateUrl: "modules/portal/team/partials/team_addNewProfile.html",
                        controller: "TeamAddNewProfileCTRL",
                        controllerAs: 'vm'
                    }
                }
            })


    };
})();
