(function(){
    'use strict';

    angular
        .module('portal.team')
        .controller('TeamRegisterCTRL', TeamRegisterCTRL);

    TeamRegisterCTRL.$inject = ['User', '$state', '$scope', '$modal', 'UserAuthorize', '$timeout', 'TeamResult'];
    function TeamRegisterCTRL(User, $state, $scope, $modal, UserAuthorize, $timeout, TeamResult){
        var vm = this;
        vm.error = false;
        vm.listUser=[];
        vm.users = [];
        vm.seleccion;
        vm.item;


        User.query({},
            function(success){
               vm.users = success;
            },
            function(error){
                vm.error = true;
            }
        );

        vm.llenar = function(){
            if(vm.listUser.indexOf(vm.seleccion) == -1){
                vm.listUser.push(vm.seleccion);
            }
        };

        vm.submit = function () {
            vm.process = true;
            $scope.modalInstance = $modal.open({
                templateUrl: 'modules/portal/team/partials/modal/register.html',
                scope: $scope,
                size: 'sm',
                backdrop: 'static',
                keyboard  : false
            });
            TeamResult.get({},
            function(success){
                vm.response = success;
                $timeout(function () {
                    if(vm.teamName != vm.response.name){
                        vm.button = true;
                        vm.process = false;
                        vm.button1 = false;
                        vm.success = true;
                        vm.failure = false;
                    }else{
                        vm.button = false;
                        vm.process =false;
                        vm.button1 = true;
                        vm.failure = true;


                    }


                }, 3000);
            },
            function(error){

            } )



        };

        vm.ok = function () {
            $scope.modalInstance.dismiss('cancel');
            $state.go('team');
        };
        vm.close = function () {
            $scope.modalInstance.dismiss('cancel');
            $state.reload();
        };
        vm.back = function () {
            $state.go('team');
        };
    }

})();