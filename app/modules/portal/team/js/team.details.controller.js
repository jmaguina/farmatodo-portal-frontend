/**
 * Created by posma-kevin on 26/04/2017.
 */
(function(){
    angular
        .module('portal.user')
        .controller('TeamDetailCTRL', TeamDetailCTRL);

    TeamDetailCTRL.$inject = ['TeamDetailService','common', '$modal','User', '$scope', 'Profile', '$timeout', 'TeamResult', 'TeamSearchProfile','Categories','Groups', 'UserSearch'];
    function TeamDetailCTRL (TeamDetailService,common, $modal, User, $scope, Profile,$timeout, TeamResult, TeamSearchProfile, Categories, Groups, UserSearch){
        var vm = this;
        vm.allUsers = [];
        vm.allProfiles = [];
        vm.user = {};
        vm.groupsSelected = [];
        vm.newUser = false;
        vm.usersGroup =[];
        vm.profilesGroup = [];
        vm.success = false;
        vm.successProfile = false;
        vm.userUpdate = {};
        vm.button = false;
        vm.exito = false;
        vm.process = false;
        vm.failure = false;
        vm.successUser = false;

        vm.profileNew = false;
        vm.searching = false;
        vm.add = false;
        vm.groupProfiles = [];
        vm.button1= false;
        vm.button2= false;
        vm.failure = false;
        vm.process = false;

        vm.tab = 1;
        vm.group = common.group;

        vm.setTab = function (tabId) {
            vm.tab = tabId;

            if(tabId == 2)
                vm.loadUsers();

            if(tabId == 3)
                vm.loadProfiles();
        };

        vm.isSet = function (tabId) {
            return vm.tab === tabId;
        };

        TeamDetailService.get(
            function (data){
                vm.usersGroup = data.users;
                vm.profilesGroup = data.profiles;
            },
            function() {
            }
        );

        User.query(
            function (data){
                vm.allUsers = data;
            },function() {
            }
        );

        Profile.query(
            function(data) {
                vm.allProfiles = data;
            },function() {
            }
        );

        vm.submit = function () {

            vm.process = true;
            $scope.modalInstance = $modal.open({
                templateUrl: 'modules/portal/team/partials/modal/team_edit_name.html',
                scope: $scope,
                size: 'sm',
                backdrop: 'static',
                keyboard  : false
            });

            TeamResult.get({},
                    function(success){
                        vm.response = success;
                        $timeout(function () {
                            if(vm.group.name != vm.response.name){
                                vm.success = true;
                                vm.process = false;
                                vm.exito = true;
                            }else{
                                vm.process =false;
                                vm.failure = true;
                            }
                        }, 3000);
                    },
                    function(error){
                        vm.process =false;
                        vm.error = true;
            });
        };

        vm.addUser = function () {
            vm.newUser = true;
        };

        vm.loadUsers = function () {
            angular.forEach(vm.allUsers, function (value,key) {
                angular.forEach(vm.usersGroup,function (usuario, key){
                    if(value.id == usuario.id) {
                        value.selected = true;
                    }
                });
            });
        };

        vm.loadProfiles =  function() {
            angular.forEach(vm.allProfiles,function (value,key) {
                angular.forEach(vm.profilesGroup, function (group, key){
                    if (value.id == group.id) {
                        value.selected = true;
                    }
                });
            });
        }

        vm.back = function () {
           vm.newUser = false;
        };

        vm.agregarUser = function() {
            $scope.modalInstance = $modal.open({
                templateUrl:'modules/portal/team/partials/modal/team_user_add.html',
                scope: $scope,
                size: 'sm',
                backdrop: 'static',
                keyboard: false
            });
        };

        vm.saveUsers = function () {
            vm.usersGroup = [];
            angular.forEach(vm.allUsers, function(value,key){
                if(value.selected == true)
                    vm.usersGroup.push(value);
            });
            vm.newUser = false;
            $scope.modalInstance.close('cancel');
        };

        vm.addProfile = function() {
            $scope.modalInstance = $modal.open({
                templateUrl:'modules/portal/team/partials/modal/team_profile_add.html',
                scope: $scope,
                size: 'sm',
                backdrop: 'static',
                keyboard: false
            });
        };

        vm.saveProfile = function() {
            vm.successProfile = true;
            vm.profilesGroup = [];
            angular.forEach(vm.allProfiles,function(value,key) {
                if(value.selected == true)
                    vm.profilesGroup.push(value);
            });
        };

        vm.removeUser = function (user) {
            vm.user = user;
            vm.success = false;
            $scope.modalInstance = $modal.open({
                templateUrl: 'modules/portal/team/partials/modal/team_user_delete.html',
                scope: $scope,
                size: 'sm',
                backdrop: 'static',
                keyboard  : false
            });
        };

        vm.removeProfile = function (profile) {
            vm.profile = profile;
            vm.success = false;
            $scope.modalInstance = $modal.open({
                templateUrl: 'modules/portal/team/partials/modal/team_profile_delete.html',
                scope: $scope,
                size: 'sm',
                backdrop: 'static',
                keyboard  : false
            });
        };

        vm.deleteUser = function () {
            vm.success = true;
            vm.usersGroup.splice(vm.usersGroup.indexOf(vm.user),1);
            vm.userUpdate = vm.allUsers[vm.allUsers.indexOf(vm.user)];
            angular.forEach(vm.allUsers, function(value,key){
                if(value.id == vm.user.id)
                    value.selected = false;
            });
            vm.loadUsers();
            $scope.modalInstance.dismiss('cancel');
            vm.success = false;
        }

        vm.ok = function() {
            vm.process = false;
            vm.exito = false;
            vm.failure = false;
            $scope.modalInstance.dismiss('cancel');
        };

        vm.close = function() {
            vm.process = false;
            vm.exito = false;
            vm.failure = false;
            $scope.modalInstance.dismiss('cancel');
        };

        /*----------------------------------Add new profile ---------------------------------------------*/

        vm.addNewProfile = function(){
            vm.profileNew = true;
          vm.groupProfiles =[];
        };
        TeamSearchProfile.query({},
            function(success){
                vm.searching = false;
                vm.profilesList = success;
            },
            function(error){
                vm.error = true;
            }
        );
        vm.searchProfiles = function(){
            vm.profilesList = [];
            vm.searching = true;
            //Simulacion de una b�squeda por filtros!
            TeamSearchProfile.query({},
                function(success){
                    $timeout(function () {
                        vm.searching = false;
                        vm.profilesList = success;

                    }, 2500);
                },
                function(error){
                    console.log("No entro");
                    vm.searching = false;
                    vm.error = true;
                }
            )
        };

        vm.submit1 = function () {
            vm.process = true;
            $scope.modalInstance = $modal.open({
                templateUrl: 'modules/portal/team/partials/modal/team_add_profile.html',
                scope: $scope,
                size: 'sm',
                backdrop: 'static',
                keyboard: false

            });
            if(vm.groupProfiles.length>0){
                vm.failure = false;
                vm.process = true;
                vm.button1= true;
                vm.button2= true;

            }else{
                vm.failure = true;
                vm.process = false;

            }

        };
        vm.back1 = function () {
            $scope.modalInstance.dismiss('cancel');
        };
        vm.back2 = function () {
            vm.profileNew = false;

        };
        vm.ok1= function(){
            vm.profileNew = false;
            $scope.modalInstance.dismiss('cancel');
        };

/*----------------------------------Add new profile ---------------------------------------------*/

/*----------------------------------modificacion pantalla new user ---------------------------------------------*/
        Categories.query({},
            function(success){
                vm.categories = success
                vm.defaultCategory = vm.categories[0].name;
            },
            function(error){
                //Manejo de Errores
            }
        );

        Groups.query({},
            function(success){
                vm.groups = success
                vm.defaultGroup = vm.groups[0].name;
            },
            function(error){
                //Manejo de Errores
            }
        );

        vm.searchUsers = function(){
            vm.userList = [];
            vm.searching = true;
            vm.searchValues ={
                "category": vm.defaultCategory,
                "group":vm.defaultGroup,
                "user": vm.user1
            }
            //Simulacion de una b�squeda por filtros!
            UserSearch.query({},vm.searchValues,
                function(success){
                    $timeout(function () {
                        vm.searching = false;
                        vm.userList = success;

                    }, 2500);
                },
                function(error){
                    vm.searching = false;
                    vm.error = true;
                }
            )
        };

/*----------------------------------modificacion pantalla new user ---------------------------------------------*/

    };

})()
