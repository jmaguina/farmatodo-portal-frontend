(function(){
    'use strict';

    angular
        .module('portal.team')
        .factory('Team', Team)
        .factory('TeamSearch', TeamSearch)
        .factory('TeamResult', TeamResult)
        .factory('TeamDetailService', TeamDetailService)
        .factory('User',User)
        .factory('Profile', Profile)
        .factory('TeamSearchProfile', TeamSearchProfile)
        .value('common',{});

    Team.$inject =['$resource','$rootScope'];
    function Team($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/static/data/teams.json');
    };
    TeamSearch.$inject =['$resource','$rootScope'];
    function TeamSearch($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/static/data/teams.json');
    };
    TeamResult.$inject = ['$resource','$rootScope'];
    function TeamResult($resource,$rootScope){
        return $resource($rootScope.domainUrl+'/static/data/teamSearch.json');
    };
    
    TeamDetailService.$inject =['$resource','$rootScope'];
    function TeamDetailService($resource,$rootScope) {
        return $resource($rootScope.domainUrl+'/static/data/teamdetails.json');
    };

    User.$inject = ['$resource','$rootScope']
    function User($resource,$rootScope) {
        return $resource($rootScope.domainUrl+'/static/data/users.json');
    };

    Profile.$inject = ['$resource','$rootScope'];
    function Profile($resource,$rootScope) {
        return $resource($rootScope.domainUrl+'/static/data/profiles.json');
    };

    TeamSearchProfile.$inject = ['$resource','$rootScope'];
    function TeamSearchProfile($resource,$rootScope) {
        return $resource($rootScope.domainUrl+'/static/data/teamListProfiles.json');
    };


    
})();
