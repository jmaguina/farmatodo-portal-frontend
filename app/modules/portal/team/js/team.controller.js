(function(){
    'use strict';

    angular
        .module('portal.team')
        .controller('TeamCTRL', TeamCTRL);

    TeamCTRL.$inject = ['Team', 'TeamSearch','$timeout', '$state', 'common'];
    function TeamCTRL(Team, TeamSearch, $timeout, $state, common){
        var vm = this;
        vm.error = false;
        vm.grupoSeleccionado = {};

        Team.query({},
            function(success){
                vm.teams = success;
            },
            function (error) {
                vm.error = true;
            }
        );

        vm.searchTeams = function(){
            vm.teams = [];
            vm.searching = true;
            vm.searchValues ={
                "name": vm.name,
                "users": vm.defaultUsers,
                "profiles": vm.defaultProfiles
                }

            //Simulacion de una b�squeda por filtros!
            TeamSearch.query({},vm.searchValues,
                function(success){
                    $timeout(function () {
                        vm.searching = false;
                        vm.teams = success;

                    }, 2500);
                },
                function(error){
                    vm.searching = false;
                    vm.error = true;
                }
            )
        };
        
        vm.createTeam = function(){
            $state.go('teamRegister');
        };

        vm.showDetail = function(group) {
            common.group = group;
            $state.go('teamDetail');
        };
      }
})();
