(function(){
	'use strict';

	angular
		.module('portal.login')
		.factory('Login', Login);

	Login.$inject = ['$resource','$rootScope'];
	function Login($resource,$rootScope){
		return $resource($rootScope.domainUrl+'/static/data/login.json');
	};
})();