(function(){
  'use strict';

  angular
  .module("portal.login", ['ui.router'])
  .run(addStateToScope)
  .config(getRoutes);


  addStateToScope.$inject = ['$rootScope', '$state', '$stateParams'];
    function addStateToScope($rootScope, $state, $stateParams){
      $rootScope.$state = $state;
      $rootScope.$stateParams = $stateParams;
    };

  getRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];
    function getRoutes($stateProvider, $urlRouterProvider){

  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('login', {
      url: '/',
      parent: 'loginView',
      views:{
        'login@': {
          templateUrl: "modules/portal/login/partials/login.html",
          controller: "LoginCTRL",
          controllerAs: 'vm'
        }
      }
    })
  };
})();