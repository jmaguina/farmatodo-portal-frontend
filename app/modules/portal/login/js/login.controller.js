(function(){
  'use strict';

  angular
    .module('portal.login')
    .controller('LoginCTRL', LoginCTRL);

  LoginCTRL.$inject = ['Login', '$state', '$scope', '$timeout', '$modal'];
  function LoginCTRL(Login, $state, $scope, $timeout, $modal){
    var vm = this;
    vm.failure = false;
    vm.process = false;
    vm.submitted = false;
    vm.success = false;
    vm.unauthorized = false;
    vm.nonexistent = false;
    vm.secondtry = false;
    vm.thirdtry = false;
    vm.blocked = false;
    vm.user = {};

    vm.login = function() {
      Login.get({},
        function(success){
          vm.authentication = success;
          vm.process = true;

          $scope.modalInstance = $modal.open({
            templateUrl: 'modules/portal/login/partials/modal/login_modal.html',
            scope: $scope,
            size: 'sm',
            backdrop: 'static',
            keyboard  : false
          });
          $timeout(function () {
            if ((vm.user.username == vm.authentication.username) && (vm.user.password == vm.authentication.password)){
              //User Blocked
              if(vm.authentication.blocked){
                vm.process = false;
                vm.blocked = true;
              }else{
                //User Authorized
                if(vm.authentication.authorized){
                  vm.process = false;
                  vm.success = true;
                }else{
                  //User Unauthorized
                  vm.process = false;
                  vm.unauthorized = true;
                }
              }
            } else {
              //User Non Existent
              if(vm.user.username != vm.authentication.username){
                vm.process = false;
                vm.nonexistent = true;
              }else{
                //Wrong Password
                if((vm.user.username == vm.authentication.username) && (vm.user.password != vm.authentication.password)){
                  //First try
                  if(vm.authentication.loginCounter == 0){
                    vm.process = false;
                    vm.failure = true;
                  }else{
                    //Second try
                    if(vm.authentication.loginCounter == 1){
                      vm.process = false;
                      vm.secondtry = true;
                    }else{
                      //Third Try
                      if(vm.authentication.loginCounter == 2){
                        vm.process = false;
                        vm.thirdtry = true;
                      }else{
                        //User Blocked
                        vm.process = false;
                        vm.blocked = true;
                      }
                    }
                  }
                }
              }
            }
          }, 2500);
        }
      )
    };

    vm.continue = function (){
      $scope.modalInstance.dismiss('cancel');
      $state.go('user');
    };

    vm.return = function (){
      $scope.modalInstance.dismiss('cancel');
      $state.reload();
    };
  };
})();