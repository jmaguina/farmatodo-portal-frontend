(function(){
  'use strict';

  angular
    .module("portal.service", ['ui.router'])
    .run(addStateToScope)
    .config(getRoutes);


  addStateToScope.$inject = ['$rootScope', '$state', '$stateParams'];
    function addStateToScope($rootScope, $state, $stateParams){
      $rootScope.$state = $state;
      $rootScope.$stateParams = $stateParams;
    };

  getRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];
    function getRoutes($stateProvider, $urlRouterProvider){

  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('service', {
      url: '/service',
      parent: 'index',
      views:{
        'content@': {
          templateUrl: "modules/portal/service/partials/service.html",
          controller: "ServiceCTRL",
          controllerAs: 'vm'
        }
      }
    })
      .state('serviceDetail', {
        url: '/serviceDetail',
        parent: 'index',
        views:{
          'content@': {
            templateUrl: "modules/portal/service/partials/service_detail.html",
            controller: "ServiceDetailCTRL",
            controllerAs: 'vm'
          }
        }
      })
  };
})();