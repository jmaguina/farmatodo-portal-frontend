(function(){
	'use strict';

	angular
		.module('portal.service')
		.factory('Function', Function)
		.factory('Service', Service)
		.value('common',{});

	Function.$inject = ['$resource','$rootScope'];
	function Function($resource,$rootScope){
		return $resource($rootScope.domainUrl+'/static/data/function.json');
	};

	Service.$inject = ['$resource','$rootScope'];
	function Service($resource,$rootScope){
		return $resource($rootScope.domainUrl+'/static/data/services.json');
	};
})();