(function(){
  'use strict';

  angular
    .module('portal.service')
    .controller('ServiceCTRL', ServiceCTRL);

  ServiceCTRL.$inject = ['Function', '$state', 'common', '$scope', '$modal'];
  function ServiceCTRL(Function, $state, common, $scope, $modal){
    var vm = this;
    vm.button = false;
    vm.error = false;
    vm.statusSuccess = true;

      Function.query({},
        function(success){
          vm.func = success;
        },
        function(error){
          vm.error = true;
        }
    );

    vm.funcStatusModal = function (func){
      vm.status = func;
      vm.button = false;
      vm.error = false;
      vm.statusSuccess = true;
      $scope.modalInstance = $modal.open({
        templateUrl:
            'modules/portal/service/partials/modal/status.html',
        scope: $scope,
        backdrop: 'static',
        size: 'sm',
        keyboard  : false
      });
    };

    vm.showDetail = function (func){
      common.func = func;
      $state.go('serviceDetail');
    };

    vm.ok = function () {
      $scope.modalInstance.dismiss('cancel');
    };

    vm.cancel = function () {
      $scope.modalInstance.dismiss('cancel');
    };

    vm.modifyStatus = function () {
      vm.statusSuccess = false;
      vm.button = true;
    };

  };
})();