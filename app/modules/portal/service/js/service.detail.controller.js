(function(){
  'use strict';

  angular
    .module('portal.service')
    .controller('ServiceDetailCTRL', ServiceDetailCTRL);

    ServiceDetailCTRL.$inject = ['Service', '$state', 'common', '$scope', '$modal'];
  function ServiceDetailCTRL(Service, $state, common, $scope, $modal){
    var vm = this;
    vm.button = false;
    vm.error = false;
    vm.statusSuccess = true;

    Service.query({},
      function(success){
        vm.services = success;
      },
      function(error){
        vm.error = true;
      }
    );

    vm.serviceStatusModal = function (service){
      vm.status = service;
      vm.button = false;
      vm.error = false;
      vm.statusSuccess = true;
      $scope.modalInstance = $modal.open({
        templateUrl:
            'modules/portal/service/partials/modal/status_service.html',
        scope: $scope,
        backdrop: 'static',
        size: 'sm',
        keyboard  : false
      });
    };

    vm.ok = function () {
      $scope.modalInstance.dismiss('cancel');
    };

    vm.cancel = function () {
      $scope.modalInstance.dismiss('cancel');
    };

    vm.modifyStatus = function () {
      vm.statusSuccess = false;
      vm.button = true;
    };

  };
})();